#ifndef STUDENT_H
#define STUDENT_H
#include <string>
using namespace std;

class Student
{
	public :
		//Constructor
		Student() //Default value
		{
			ID = 0;
			Name = "No Data";
		}
		//Mutator
		void setData(string name, int id) //set Name and ID
		{
			Name = name;
			ID = id;
		}
		//Accessors 
		int getID() //Show ID
		{
			return ID;
		}
		string getName() //Show Name
		{
			return Name;
		}

private :
	int ID; 
	string Name;
};
#endif