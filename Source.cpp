#include <iostream>
#include <list>
#include <string>
#include "student.h" //include student header file
using namespace std;

int HashID(int id); //Function that find the hash index by seaching id
string searchID(list <Student> ISNE[], int searchID); //Seaching in the list by id and show Name
int HashName(string name); //Function that find the hash index by seaching name
int searchName(list <Student> ISNE[], string searchName); //Seaching in the list by Name and show id

int main()
{
	list <Student> ISNE[50]; //Declare list for store data from information of ISNE students
	int type, id;
	string name;

	//Set some data to show Demo program
	Student Data_1;
	Data_1.setData("Kanokpol", 600611001);
	Student Data_2;
	Data_2.setData("Same", 600611002);
	Student Data_3;
	Data_3.setData("KenCosh", 590611001);
	Student Data_4;
	Data_4.setData("Plan", 590611002);

	//Bring data into the list 
	ISNE[HashID(Data_1.getID())].push_back(Data_1);
	ISNE[HashID(Data_2.getID())].push_back(Data_2);
	ISNE[HashID(Data_3.getID())].push_back(Data_3);
	ISNE[HashID(Data_4.getID())].push_back(Data_4);
	ISNE[HashName(Data_1.getName())].push_back(Data_1);
	ISNE[HashName(Data_2.getName())].push_back(Data_2);
	ISNE[HashName(Data_3.getName())].push_back(Data_3);
	ISNE[HashName(Data_4.getName())].push_back(Data_4);

	//Show menu what users need to do
	cout << "Input... [1] = Search by Student ID , [2] = Search by Student Name : ";
	cin >> type;
	
	//If user not input 1 or 2 show try again message
	while (type != 1 && type != 2)
	{
		cout << "\nWrong selected ,Try again! : ";
		cin >> type;
	}
	
	//If user press 1 that mean find by ID or 2 find by name
	switch (type)
	{
	case 1 : 
		cout << "\nInput Student ID (such as 600611001) : ";
		cin >> id;
		cout << "\nYour Student ID is " << id << endl;
		cout << "Your Name is " << searchID(ISNE, id);
		break;
	case 2 : 
		cout << "\nInput Student Name (such as Kanokpol) : ";
		cin >> name;
		cout << "\nYour Student Name is " << name << endl;
		cout << "Your ID is " << searchName(ISNE, name);
		break;
	default: break;
	}

	cout << endl << endl;
	system("PAUSE");
	return 0;
}

int HashID(int id)
{
	return id % 50; //Modular by 50 because array list have 0 - 49
}

int HashName(string name)
{
	int sum = 0;
		//Loop for each character convert to ASCII and sum together 
		for (int i = 0; i < name.length(); i++)
		{
			char eachchar = name.at(i);
			sum = sum + int(eachchar);
		}
	
return sum % 50; //Modular by 50 because array list have 0 - 49
}

string searchID(list <Student> ISNE[], int searchID)
{
	int hashindex = HashID(searchID); //Finding hash index
	list <Student>::iterator it; //iterator to point to the list
	it = ISNE[hashindex].begin(); //Make iterator point to the list
	while (it != ISNE[hashindex].end()) 
	{
		if (searchID == it->getID()) //Compare between what ID that user looking for with data in the list
		{
			return it->getName(); //Show Name
		}
		else 
			it++; //Go next list in same index
	}
	if (it == ISNE[hashindex].end()) //Not found show this messages
	{
		return "No data!";
	}
}

int searchName(list <Student> ISNE[], string searchName)
{
	int hashindex = HashName(searchName);
	list <Student>::iterator it;
	it = ISNE[hashindex].begin();
	while (it != ISNE[hashindex].end())
	{
		if (searchName == it->getName()) //Compare between what Name that user looking for with data in the list
		{
			return it->getID(); //Show ID
		}
		else
			it++;  //Go next list in same index
	}
	if (it == ISNE[hashindex].end()) //Not found show this messages
	{
		cout << "No Data!";
		return NULL;
	}
}